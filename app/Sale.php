<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sale extends Model
{
    public function shop()
    {
        return $this->belongsTo('App\Shop');
    }
    
    public function member()
    {
        return $this->belongsTo('App\Member');
    }

    public function commentLog()
    {
        return $this->belongsTo('App\Commentlog');
    }
}
