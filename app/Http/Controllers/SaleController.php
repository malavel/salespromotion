<?php

namespace App\Http\Controllers;

use App\Sale;
use App\Shop;
use App\Member;
use Illuminate\Http\Request;

class SaleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->filled('keyword')){
            $keyword = $request->input('keyword');
            $message = 'セール一覧(検索キーワード：'.$keyword.')';
            $sales = Sale::where('sale_content', 'like', '%' . $keyword . '%' )->get();
        } else {
            $message = 'セール一覧';
            $sales = Sale::where('avail_flg', '=', 0)->orderBy('startDate', 'asc')->get();
        }
        return view('index' , ['message' => $message, 'sales' => $sales]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $sale = new Sale;
        $shops = Shop::all()->pluck('name', 'id');
        $members = Member::all()->pluck('user_name', 'id');
        $avail = 0;
        return view('new', ['sale' => $sale, 'shops' => $shops, 'members' => $members]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $sale = new Sale();
        $sale->shop_id = $request->shop_id;
        $sale->sale_title = $request->sale_title;
        $sale->sale_content = $request->sale_content;
        $sale->user_id = $request->user_id;
        $sale->startDate = $request->startDate;
        $sale->endDate = $request->endDate;
        $sale->guerrilla_flg = $request->guerrilla_flg;
        $sale->exec_flg = $request->exec_flg;
        $sale->avail_flg = 0;
        $sale->save();
        return redirect()->route('sale.show', ['id' => $sale->id]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Sale  $sale
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id, Sale $sale)
    {
        //
        $sale = Sale::find($id);
        return view('show', ['sale' => $sale]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Sale  $sale
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id, Sale $sale)
    {
        $sale = Sale::find($id);
        $shops = Shop::all()->pluck('name', 'id');
        $members = Member::all()->pluck('user_name', 'id');
        return view('edit', ['sale' => $sale, 'shops' => $shops, 'members' => $members]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Sale  $sale
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id, Sale $sale)
    {
        $sale = Sale::find($id);

        $sale->shop_id = $request->shop_id;
        $sale->sale_title = $request->sale_title;
        $sale->sale_content = $request->sale_content;
        $sale->user_id = $request->user_id;
        $sale->startDate = $request->startDate;
        $sale->endDate = $request->endDate;
        $sale->guerrilla_flg = $request->guerrilla_flg;
        $sale->exec_flg = $request->exec_flg;
        $sale->avail_flg = 0;
        $sale->save();
        return redirect()->route('sale.show', ['id' => $sale->id]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Sale  $sale
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $sale = Sale::find($id);
        $sale->delete();
        return redirect('/salelist');

    }
}
