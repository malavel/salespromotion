<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    // return view('welcome');
    return redirect('/salelist');
});

Route::get('/salelist', 'SaleController@index')->name('sale.list');
Route::get('/salelist/new', 'SaleController@create')->name('sale.new');
Route::post('/salelist', 'SaleController@store')->name('sale.store');
Route::get('/salelist/edit/{id}', 'SaleController@edit')->name('sale.edit');
Route::post('/salelist/update/{id}', 'SaleController@update')->name('sale.update');
Route::get('/salelist/{id}', 'SaleController@show')->name('sale.show');
Route::delete('/salelist/{id}', 'SaleController@destroy')->name('sale.delete');