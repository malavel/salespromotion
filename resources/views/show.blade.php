@extends('layouts.layout')
@section('title','sale detail')

@section('content')
    <h1 class="content_h1title">セール詳細</h1>

    <div class="content_inner uk-flex uk-flex-around uk-flex-middle uk-flex-wrap">
        <div class="">
            <table class="uk-table uk-table-divider">
                <tr>
                    <th>店舗</th>
                    <td>{{ $sale->shop->name }}</td>
                </tr>
                <tr>
                    <th>セール内容</th>
                    <td>{{ $sale->sale_title }}</td>
                </tr>
                <tr>
                    <th>開始時間</th>
                    <td>{{ $sale->startDate }}</td>
                </tr>
                <tr>
                    <th>終了時間</th>
                    <td>{{ $sale->endDate }}</td>
                </tr>
                <tr>
                    <th>セール詳細</th>
                    <td>{{ $sale->sale_content }}</td>
                </tr>
                <tr>
                    <td>
                    @if ($sale->guerrilla_flg != 0 )
                        <span class="uk-label uk-label-warning span-label">ゲリラ</span><br>
                    @endif
                    </td>
                    <td>
                    @if ($sale->exec_flg != 0)
                        <span class="uk-label uk-label-danger span-label">セール中</span>
                    @endif
                    </td>
                </tr>
            </table>
        </div>
        <div class="uk-card uk-card-default uk-card-body uk-margin-left uk-animation-scale-up">
            <p><a href={{ route('sale.list') }} class="uk-button uk-button-default">一覧に戻る</a></p>
            <div class="uk-button-group">
                <a href={{ route('sale.edit', ["id" => $sale->id]) }} class="uk-button uk-button-primary">編集</a>
                {{ Form::open(['method' => 'delete', 'route' => ['sale.delete', $sale->id]]) }}
                    {{ Form::submit('削除', ['class' => 'uk-button uk-button-danger ml-5', 'onclick' => 'return deleteCheck();']) }}
                {{ Form::close() }}
            </div>
        </div>
    </div>

    <!-- 削除確認ダイアログ -->
    <script>
        function deleteCheck() {
            var check = confirm('ほんとのほんとのほんとに削除してもOKですか？');
            return check;
        }
    </script>
        

@endsection