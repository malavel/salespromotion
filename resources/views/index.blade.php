@extends('layouts.layout')
@section('title','sale list')

@section('content')
    <h1 class="sale-list-h1">{{ $message }}</h1>
    <!-- sale-list-table -->
    <div class="uk-margin-bottom" id="sale-list-table">
        <table class="uk-table uk-table-striped uk-table-hover">
            <tr>
                <th></th>
                <th>店舗</th>
                <th>セール内容</th>
                <th>開始時間</th>
                <th>終了時間</th>
                <th></th>
            </tr>
            @foreach ($sales as $sale)
            <tr>
                <td>
                    @if ($sale->guerrilla_flg != 0 )
                        <span class="uk-label uk-label-warning span-label">ゲリラ</span><br>
                    @endif
                    @if ($sale->exec_flg != 0)
                        <span class="uk-label uk-label-danger span-label">セール中</span>
                    @endif
                </td>
                <td>{{ $sale->shop->name }}</td>
                <td>{{ $sale->sale_title }}</td>
                <td>{{ $sale->startDate }}</td>
                <td>{{ $sale->endDate }}</td>
                
                <td>
                    <div class="uk-button-group">
                        <a href='{{ route("sale.show", ["id" => $sale->id]) }}' class="uk-button uk-button-primary uk-button-small">詳細</a>
                        {{ Form::open(['method' => 'delete', 'route' => ['sale.delete', $sale->id]]) }}
                            {{ Form::submit('削除', ['class' => 'uk-button uk-button-danger uk-button-small ml-5', 'onclick' => 'return deleteCheck();']) }}
                        {{ Form::close() }}
                    </div>
                </td>
            </tr>
            @endforeach
        </table>
    </div>
    <!-- ./ sale-list-table -->

    <!-- sale-list-button -->
    <div class="uk-margin-bottom" id="sale-list-button">
        <a href="{{ route('sale.new') }}" class="uk-button uk-button-default">新規追加</a>
    </div>
    <!-- ./sale-list-button -->
    
    <!-- 削除確認ダイアログ -->
    <script>
        function deleteCheck() {
            var check = confirm('ほんとのほんとのほんとに削除してもOKですか？');
            return check;
        }
    </script>
@endsection