<!DOCTYPE html>
<html>
    <head>
        <meta charset='utf-8'>
        <title>@yield('title')</title>
        @include('layouts.style-sheet')
    </head>
    <body>
        @include('layouts.nav')
        <div class="uk-container">
            @yield('content')
        </div>
        @include('layouts.javascript')
    </body>
</html>