@extends('layouts.layout')
@section('title','新規登録')

@section('content')
    {!! Form::open(['route' => 'sale.store', 'class' => 'uk-form uk-form-horizontal uk-margin-large']) !!}

    <div class="uk-margin">
        {!! Form::label('shop_id', '店舗名:', ['class' => 'uk-form-label'])!!}
        <div class="uk-form-controls">
            {!! Form::select('shop_id', $shops, null, ['class' => 'uk-select']) !!}
        </div>
    </div>

    <div class="uk-margin">
        {!! Form::label('sale_title', 'セールタイトル:', ['class' => 'uk-form-label']) !!}
        <div class="uk-form-controls">
            {!! Form::text('sale_title',null, ['class' => 'uk-input']) !!}
        </div>
    </div>

    <div class="uk-margin">
        {!! Form::label('sale_content', 'セール内容:', ['class' => 'uk-form-label']) !!}
        <div class="uk-form-controls">
            {!! Form::textarea('sale_content',null, ['class' => 'uk-textarea']) !!}
        </div>
    </div>

    <div class="uk-margin">
        {!! Form::label('startDate', '開始日時:', ['class' => 'uk-form-label'])!!}
        <div class="uk-form-controls">
            {!! Form::text('startDate',null, ['class' => 'uk-input flatpickr', 'readonly' => 'readonly']) !!}
        </div>
    </div>

    <div class="uk-margin">
        {!! Form::label('endDate', '終了日時:', ['class' => 'uk-form-label'])!!}
        <div class="uk-form-controls">
            {!! Form::text('endDate',null, ['class' => 'uk-input flatpickr', 'readonly' => 'readonly']) !!}
        </div>
    </div>

    <div class="uk-margin">
        {!! Form::label('guerrilla_flg', 'ゲリラならCheck:', ['class' => 'uk-form-label'])!!}
        <div class="uk-form-controls">
            {!! Form::checkbox('guerrilla_flg', 1, false, ['class' => 'uk-checkbox']) !!}
        </div>
    </div>

    <div class="uk-margin">
    {!! Form::label('exec_flg', '実行中ならCheck:', ['class' => 'uk-form-label'])!!}
        <div class="uk-form-controls">
            {!! Form::checkbox('exec_flg', 1, false, ['class' => 'uk-checkbox']) !!}
        </div>
    </div>

    <div class="uk-margin">
    {!! Form::label('user_id', '登録者:', ['class' => 'uk-form-label'])!!}
        <div class="uk-form-controls">
            {!! Form::select('user_id', $members, null, ['class' => 'uk-select']) !!}
        </div>
    </div>

    <div class="uk-margin">
        {!! Form::submit('登録', ['class' => 'uk-button uk-button-primary']) !!}
    </div>

    {!! Form::close() !!}

    <script>
        const config = {
            enableTime: true,
            dateFormat: "Y-m-d H:i",
            minDate: "today"
        }
        flatpickr('.flatpickr', config)
    </script>
@endsection